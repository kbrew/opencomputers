#!/usr/bin/env python3

import sys
import re

from pprint import pprint

file_name = sys.argv[1]

class Vividict(dict):
    def __missing__(self, key):
        value = self[key] = type(self)()
        return value

recipes = Vividict()

with open(file_name, "r") as file:
    for line in file:
        match = re.match(r"recipedumper:(?P<recipe_type>\w+)\!(?P<oredict_item>@\w+)->(?P<recipe_items>.*)", line)
        if not match:
            print("invalid recipe")
            print(line)
            continue

        
