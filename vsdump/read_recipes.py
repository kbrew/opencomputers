#!/usr/bin/env python3

import sys
import re

from pprint import pprint

file_name = sys.argv[1]

class Vividict(dict):
    def __missing__(self, key):
        value = self[key] = type(self)()
        return value

recipes = Vividict()

with open(file_name, "r") as file:
    for line in file:
        match = re.match(r"recipedumper:(?P<recipe_type>\w+)\!(?P<recipe_shape>\(w=\d+,h=\d+\))?(?P<recipe_input>.*)->(?P<recipe_output>.*)", line)
        if not match:
            print("invalid recipe")
            print(line)
            continue
        recipe_type = match.group("recipe_type")
        recipe_input = match.group("recipe_input")
        recipe_output = match.group("recipe_output")
        recipe_shape = match.group("recipe_shape")

        if recipe_shape:
            match = re.match(r"\(w=(?P<width>\d+),h=(?P<height>\d+)\)", recipe_shape)
            recipe_width = int(match.group("width"))
            recipe_height = int(match.group("height"))
        
        items = ([{"id": m.group("item_id"), "quantity": int(m.group("item_quantity"))} 
                  for m in 
                    re.finditer(r"\((?P<item_id>[^,]+),(?P<item_quantity>\d+)\)", recipe_input)])

        match = re.match(r"\((?P<item_id>[^,]+),(?P<item_quantity>\d+)\)", recipe_output)
        if not match:
            print("invalid output")
            print(line)
            continue

        output_item = match.group("item_id")
        output_quantity = int(match.group("item_quantity"))
        
        recipes[output_item] = {"type": recipe_type, \
                                "id": output_item, \
                                "quantity": output_quantity, \
                                "inputs": items}
        if recipe_shape:
            recipes[output_item]["shape"] = {"w":recipe_width, "h":recipe_height}


pprint(recipes)
