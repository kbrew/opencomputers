local math = require("math")
local string = require("string")

local component = require("component")
local sides = require("sides")

local inv = component.inventory_controller

local slot = 1
local item = inv.getStackInInternalSlot(slot)

for k,v in pairs(inv) do
  io.write(string.format("%s -> %s", k, v))
end

if item then
  print("Item name: ", item.name)
  print("Item count: ", item.size)
  print("Item damage: ", item.damage)
else
  print("Slot " .. slot .. " is empty")
end